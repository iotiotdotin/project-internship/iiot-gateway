## Modbus as an “Open Protocol”

Depending on the process automation equipment manufacturer, a very specific or proprietary language is used or it may be a language that is commonly open to the industry. It is these open protocols many manufacturers adapt to easily integrate their products in a market.


An “Open protocol” means the specifications are published and may be used by anyone freely or by license.

Open protocols are usually backed by a combination of corporations, user groups, professional societies, and governments. This provides users with a much wider choice of devices or systems that can be utilized to meet specific applications.

Advantages of open protocols include support by multiple manufacturers, software vendors, and install or service organizations, active community groups for support, the ability to stay current and add capabilities in the future.


The Modbus communication protocol is the oldest and by far the most popular automation protocol in the field of process automation and SCADA (Supervisory Control and Data Acquisition).

Modbus is a communications protocol published by Modicon in 1979 for use with its Programmable Logic Controllers (PLCs). Modicon is now owned by Schneider Electric.

Modbus provides a common language for devices and equipment to communicate with one and another.

For example, Modbus enables devices on a system that measures temperature and humidity connected on the same network to communicate the results to a supervisory computer or PLC.

## Types of Modbus Communication Protocol

Several versions of the Modbus protocol exist for the serial port and Ethernet and the most common are:

– Modbus RTU

– Modbus ASCII

– Modbus TCP

– Modbus Plus


Modicon published the Modbus communication interface for a multidrop network based on a Master-Slave architecture.

Communication between the Modbus nodes is achieved with send request and read response type messages.

Modbus communicates over several types of physical media such as:

– Serial RS-232

– Serial RS-485

– Serial RS-422

– Ethernet


The original Modbus interface ran on RS-232 serial communication, but most of the later Modbus implementations use RS-485 because it allowed:

– Longer distances.

– Higher speeds.

– The possibility of multiple devices on a single multi-drop network.


Master-Slave Modbus communication over serial RS-485 physical media using two-wire transmit and receive connections.

## Introduction to Modbus Message Structure

The main Modbus message structure is Peer-to-Peer. Modbus is able to function on both Point-to-Point and Multidrop networks.

Modbus devices communicate using a Master-Slave (Client-Server for Ethernet) technique in which only one device (the Master/Server) can initiate transactions (called queries).

The other devices (Slaves/Clients) respond by supplying the requested data to the master, or by taking the action requested in the query.

Masters can address individual slaves or initiate a broadcast message to all slaves. Slaves return a response to all message queries addressed to them individually, but do not respond to broadcast messages.

Slaves do not initiate messages on their own and only respond to message queries transmitted from the master.

The master’s query will consist of:

– Slave address (broadcast address).

– Function code with a read or write data command to the slave.

– The write command “Data” if a write command was initiated by the master.

– Error checking field.

A slave’s response consists of:

– Fields confirming it received the request.

– The data to be returned.

– Error checking data.

If no error occurs, the slave’s response contains the data as requested.

## PLC (Programmable Logic Controllers)

PLC stands for “Programmable Logic Controller”. A PLC is a computer specially designed to operate reliably under harsh industrial environments – such as extreme temperatures, wet, dry, and/or dusty conditions. PLCs are used to automate industrial processes such as a manufacturing plant’s assembly line, an ore processing plant, or a wastewater treatment plant.

The biggest differences are that a PLC can perform discrete and continuous functions that a PC cannot do, and a PLC is much better suited to rough industrial environments. A PLC can be thought of as a ‘ruggedized’ digital computer that manages the electromechanical processes of an industrial environment.PLCs play a crucial role in the field of automation, using forming part of a larger SCADA system.

--Imagine you have a light connected to a switch. In general, the light operates under two conditions – ON and OFF. Now you are given a task that when you turn ON the switch, the light should glow only after 30 seconds. With this hard-wired setup – we’re stuck. The only way to achieve this is to completely rewire our circuit to add a timing relay. That’s a lot of hassle for a minor change.

This is where a programmable logic controller comes into the picture, which doesn’t require any additional wiring and hardware to make sure of a change. Rather it requires a simple change in code, programming the PLC to only turn on the light 30 seconds after the switch is turned ON. So, by using a PLC, it is easy to incorporate multiple inputs and outputs.

![PLC](https://cdn.instrumentationtools.com/wp-content/uploads/2016/07/instrumentationtools.com_what-is-plc.png)

A PLC Scan Process includes the following steps

-The operating system starts cycling and monitoring of time.
-The CPU starts reading the data from the input module and checks the status of all the inputs.
-The CPU starts executing the user or application program written in relay-ladder logic or any other PLC-programming language.
-Next, the CPU performs all the internal diagnosis and communication tasks.
-According to the program results, it writes the data into the output module so that all outputs are updated.
-This process continues as long as the PLC is in run mode.

There are a few key features that set PLCs apart from industrial PCs, microcontrollers, and other industrial control solutions:

• I/O – The PLC’s CPU stores and processes program data, but input and output modules connect the PLC to the rest of the machine; these I/O modules are what provide information to the CPU and trigger specific results. I/O can be either analog or digital; input devices might include sensors, switches, and meters, while outputs might include relays, lights, valves, and drives. Users can mix and match a PLC’s I/O in order to get the right configuration for their application.
• Communications – In addition to input and output devices, a PLC might also need to connect with other kinds of systems; for example, users might want to export application data recorded by the PLC to a supervisory control and data acquisition (SCADA) system, which monitors multiple connected devices. PLCs offer a range of ports and communication protocols to ensure that the PLC can communicate with these other systems.
• HMI – In order to interact with the PLC in real time, users need an HMI, or Human Machine Interface. These operator interfaces can be simple displays, with a text-readout and keypad, or large touchscreen panels more similar to consumer electronics, but either way, they enable users to review and input information to the PLC in real time.


## The most common used programming for PLC is LADDER LOGIC
Ladder logic is the simplest form of PLC programming. It is also known as “relay logic”. The relay contacts used in relay controlled systems are represented using ladder logic.

![PLC](https://www.electrical4u.com/wp-content/uploads/Ladder-Logic.png)

-- In the above example, two pushbuttons are used to control the same lamp load. When any one of the switches is closed, the lamp will glow. The two horizontal lines are called rungs and two vertical lines are called rails. Every rung forms the electrical connectivity between Positive rail (P) and Negative rail (N). This allows the current to flow between input and output devices.

