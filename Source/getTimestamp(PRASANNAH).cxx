#include<stdio.h>  
#include<stdlib.h>
#include<time.h>  // libary file to execute time functions

int main()
{

time_t rawtime;      // with the help of header file storing datas in rawtime 
struct tm* timeinfo; // structure contains day,month,etc.,
time(&rawtime);    
timeinfo=localtime(&rawtime); 
printf("%d-%02d-%d %02d:%02d:%02d \n",timeinfo->tm_year+1900,timeinfo->tm_mday,timeinfo->tm_mon+1,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
// by just modifying the printf statement we can display nay format of timestamp 
}